#!/bin/sh

# $1 -- output file

if [ "z$1" = "z" ]; then
  echo "WTF?!"
  exit 1
fi

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"
mdir=`pwd`
srcdir="$mdir/../images"
cd "$odir"

sh "${mdir}/genimg_normal.sh" "$1"
