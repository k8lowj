#!/bin/sh

# $1 -- output file
# $2 -- have librsvg?

if [ "z$1" = "z" ]; then
  echo "WTF?!"
  exit 1
fi

have_svg="tan"
if [ "z$2" = "z" ]; then
  have_svg="ona"
fi

odir=`pwd`
mdir=`dirname "$0"`
cd "$mdir"
mdir=`pwd`
srcdir="$mdir/../images"
cd "$odir"

#pixmap_DATA = logjam_goat.png logjam_pencil.png

IMAGES="logjam_ljuser.png logjam_ljcomm.png logjam_private.png logjam_protected.png logjam_pencil.png logjam_rarrow.png logjam_larrow.png logjam_throbber_1.png logjam_throbber_2.png logjam_throbber_3.png logjam_throbber_4.png logjam_throbber_5.png logjam_throbber_6.png logjam_throbber_7.png logjam_throbber_8.png logjam_lrarrow.png logjam_goat.png"

LIST=" \
  logjam_ljuser        ${srcdir}/logjam_ljuser.png \
  logjam_ljcomm        ${srcdir}/logjam_ljcomm.png \
  logjam_private       ${srcdir}/logjam_private.png \
  logjam_protected     ${srcdir}/logjam_protected.png \
  logjam_rarrow        ${srcdir}/logjam_rarrow.png \
  logjam_larrow        ${srcdir}/logjam_larrow.png \
  logjam_lrarrow       ${srcdir}/logjam_lrarrow.png"


# images that are larger; we RLE encode them.
BIG_LIST=" \
  logjam_pencil        ${srcdir}/logjam_pencil.png       \
  logjam_goat          ${srcdir}/logjam_goat.png"

THROBBER_LIST=" \
  logjam_throbber_1    ${srcdir}/logjam_throbber_1.png   \
  logjam_throbber_2    ${srcdir}/logjam_throbber_2.png   \
  logjam_throbber_3    ${srcdir}/logjam_throbber_3.png   \
  logjam_throbber_4    ${srcdir}/logjam_throbber_4.png   \
  logjam_throbber_5    ${srcdir}/logjam_throbber_5.png   \
  logjam_throbber_6    ${srcdir}/logjam_throbber_6.png   \
  logjam_throbber_7    ${srcdir}/logjam_throbber_7.png   \
  logjam_throbber_8    ${srcdir}/logjam_throbber_8.png"

#
# noinst_DATA = pixbufs.h
# CLEANFILES = ${noinst_DATA}
#
# if WITH_RSVG
# pixbufs.h: ${IMAGES}
#   gdk-pixbuf-csource --raw --build-list ${LIST} >${srcdir}/pixbufs.h
#   gdk-pixbuf-csource --rle --build-list ${BIG_LIST} >>${srcdir}/pixbufs.h
# else
# pixbufs.h: ${IMAGES}
#   gdk-pixbuf-csource --raw --build-list ${LIST} >${srcdir}/pixbufs.h
#   gdk-pixbuf-csource --rle --build-list ${THROBBER_LIST} ${BIG_LIST} >>${srcdir}/pixbufs.h
# endif


if [ "$have_svg" = "tan" ]; then
  echo "generating SVG pixbufs..."
  gdk-pixbuf-csource --raw --build-list ${LIST} >"$1"
  gdk-pixbuf-csource --rle --build-list ${BIG_LIST} >>"$1"
else
  echo "generating non-SVG pixbufs..."
  gdk-pixbuf-csource --raw --build-list ${LIST} >"$1"
  gdk-pixbuf-csource --rle --build-list ${THROBBER_LIST} ${BIG_LIST} >>"$1"
fi
