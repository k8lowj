/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_CHECKFRIENDS_H__
#define __LOGJAM_CHECKFRIENDS_H__

#include <glib-object.h>
#include "gtk-all.h"

#include "account.h"


typedef enum {
  CF_DISABLED,
  CF_ON,
  CF_NEW
} CFState;

typedef struct _CFMgr CFMgr;
typedef struct _CFMgrClass CFMgrClass;


#define LOGJAM_TYPE_CFMGR          (cfmgr_get_type())
#define LOGJAM_CFMGR(object)       (G_TYPE_CHECK_INSTANCE_CAST((object), LOGJAM_TYPE_CFMGR, CFMgr))
#define LOGJAM_CFMGR_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST ((klass), LOGJAM_TYPE_CFMGR, CFMgrClass))

extern CFMgr *cfmgr_new (JamAccount *acc);
extern GType cfmgr_get_type (void);

extern JamAccountLJ *cfmgr_get_account (CFMgr *cfm);

extern void cfmgr_set_mask (CFMgr *cfm, guint32 mask);

extern void cfmgr_set_account (CFMgr *cfm, JamAccount *acc);
extern void cfmgr_set_state (CFMgr *cfm, CFState state);
extern CFState cfmgr_get_state (CFMgr *cfm);

extern void cf_threshold_normalize (gint *threshold);

extern gboolean checkfriends_cli (JamAccountLJ *acc);
extern void checkfriends_cli_purge (JamAccountLJ *acc);

#ifdef HAVE_GTK
typedef struct _CFFloat CFFloat;

extern CFFloat *cf_float_new (CFMgr *cfm);
extern void cf_float_decorate_refresh (void);
extern void cf_app_update_float (void);

#ifdef USE_DOCK
extern void cf_update_dock (CFMgr *cfm, GtkWindow *parent);
#endif
#endif /* HAVE_GTK */


#endif
