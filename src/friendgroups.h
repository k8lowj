/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_FRIENDGROUPS_H__
#define __LOGJAM_FRIENDGROUPS_H__

#include "gtk-all.h"

#include "liblj/friends.h"


extern void friendgroups_dialog_new (GtkWindow *parent, JamAccountLJ *acc, GSList *friends);


#endif
