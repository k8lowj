/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_USERLABEL_H__
#define __LOGJAM_USERLABEL_H__


#define JAM_USER_LABEL(obj)  (G_TYPE_CHECK_INSTANCE_CAST((obj), jam_user_label_get_type(), JamUserLabel))

typedef struct _JamUserLabel JamUserLabel;

extern GtkWidget *jam_user_label_new (void);
extern GType jam_user_label_get_type (void);

extern void jam_user_label_set_account (JamUserLabel *jul, JamAccount *acc);
extern void jam_user_label_set_journal (JamUserLabel *jul, const char *journalname);


#endif
