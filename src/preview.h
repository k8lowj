/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2005 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_PREVIEW_H__
#define __LOGJAM_PREVIEW_H__

#include <gtkhtml/gtkhtml.h>

#include "liblj/livejournal.h"

#include "jam.h"

typedef struct _HTMLPreview HTMLPreview;

typedef LJEntry *(*GetEntryFunc) (HTMLPreview *hp);

struct _HTMLPreview {
  GtkHTML html; /* parent */
  GetEntryFunc get_entry;
  gpointer get_entry_data;
};

typedef struct _PreviewUI PreviewUI;
struct _PreviewUI {
  GtkWidget *win;
  HTMLPreview *html;
  JamWin *jw;
};


extern GtkWidget *html_preview_new (GetEntryFunc get_entry, gpointer get_entry_data);
extern void preview_ui_show (JamWin *jw);
extern void preview_update (HTMLPreview *hp);


#endif
