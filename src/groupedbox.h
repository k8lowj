/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_GROUPEDBOX_H__
#define __LOGJAM_GROUPEDBOX_H__


#define TYPE_GROUPEDBOX  groupedbox_get_type()
#define GROUPEDBOX(obj)  (G_TYPE_CHECK_INSTANCE_CAST((obj), TYPE_GROUPEDBOX, GroupedBox))


typedef struct _GroupedBox GroupedBox;
typedef struct _GroupedBoxClass GroupedBoxClass;

/* +--------+
 * | header +
 * +-+------+
 * | |      |\
 * | | vbox |  body
 * | |      |/
 * +-+------+
 */
struct _GroupedBox {
  GtkVBox parent;
  GtkWidget *vbox;
  GtkWidget *header, *body; /* for foldbox subclass */
};


struct _GroupedBoxClass {
  GtkVBoxClass parent_class;
};


extern GType groupedbox_get_type (void);

extern GtkWidget *groupedbox_new (void);
extern GtkWidget *groupedbox_new_with_text (const char *text);
extern void groupedbox_set_header_widget (GroupedBox *b, GtkWidget *w);
extern void groupedbox_set_header (GroupedBox *b, const char *title, gboolean bold);
extern void groupedbox_pack (GroupedBox *b, GtkWidget *w, gboolean expand);
extern GType groupedbox_get_type (void);


#endif
