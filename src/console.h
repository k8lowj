/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2004 Evan Martin <martine@danga.com>
 */
#ifndef __LOGJAM_CONSOLE_H__
#define __LOGJAM_CONSOLE_H__

#include "account.h"


extern void console_dialog_run (GtkWindow *parent, JamAccountLJ *acc);


#endif /* __LOGJAM_CONSOLE_H__ */
