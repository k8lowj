/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_LOGIN_H__
#define __LOGJAM_LOGIN_H__

#include "account.h"


extern JamAccount *login_dlg_run (GtkWindow *parent, JamHost *host, JamAccount *acc);
extern gboolean login_run (GtkWindow *parent, JamAccountLJ *acc);
extern gboolean login_check_lastupdate (GtkWindow *parent, JamAccountLJ *acc);


#endif
