/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_DATESEL_H__
#define __LOGJAM_DATESEL_H__

#include <gtk/gtkoptionmenu.h>


#define DATESEL(obj)  (G_TYPE_CHECK_INSTANCE_CAST((obj), datesel_get_type(), DateSel))

typedef struct _DateSel DateSel;

extern GType datesel_get_type (void);
extern GtkWidget *datesel_new (void);

extern void datesel_get_tm (DateSel *ds, struct tm *ptm);
extern void datesel_set_tm (DateSel *ds, struct tm *ptm);
extern gboolean datesel_get_backdated (DateSel *ds);
extern void datesel_set_backdated (DateSel *ds, gboolean backdated);


#endif /* datesel_h */
