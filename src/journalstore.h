/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_JOURNALSTORE_H__
#define __LOGJAM_JOURNALSTORE_H__

#include "liblj/livejournal.h"

#include "account.h"

#define MAX_MATCHES  (1000)


typedef struct _JournalStore JournalStore;


extern JournalStore *journal_store_open (JamAccount *acc, gboolean create, GError **err);
extern gboolean journal_store_reindex (JamAccount *acc, GError **err);

extern JamAccount *journal_store_get_account (JournalStore *js);

extern void journal_store_free (JournalStore *js);

extern gboolean journal_store_put (JournalStore *js, LJEntry *entry, GError **err);
extern gboolean journal_store_put_group (JournalStore *js, LJEntry **entries, int c, GError **err);

extern time_t journal_store_lookup_entry_time (JournalStore *js, int itemid);
extern guint32 journal_store_get_month_entries (JournalStore *js, int year, int mon);
extern LJEntry *journal_store_get_entry (JournalStore *js, int get_itemid);
extern int journal_store_get_latest_id (JournalStore *js);
extern int journal_store_get_count (JournalStore *js);
extern gboolean journal_store_get_invalid (JournalStore *js);

extern char *journal_store_get_lastsync (JournalStore *js);
extern gboolean journal_store_put_lastsync (JournalStore *js, const char *lastsync, GError **err);

typedef void (*JournalStoreSummaryCallback) (int itemid, time_t etime, const char *summary, LJSecurity *sec, gpointer data);

extern gboolean journal_store_get_day_entries (JournalStore *js, int year, int mon, int day, JournalStoreSummaryCallback cb_func, gpointer cb_data);

extern gboolean journal_store_find_relative_by_time (JournalStore *js, time_t when, int *ritemid, int dir, GError *err);
extern gboolean journal_store_find_relative (JournalStore *js, gint itemid, int *ritemid, int dir, GError *err);

typedef gboolean(*JournalStoreScanCallback) (const char *str, gpointer data);

extern gboolean journal_store_scan(JournalStore *js,
                                   JournalStoreScanCallback scan_cb,
                                   const gpointer scan_data, JournalStoreSummaryCallback cb_func, const gpointer cb_data);

#endif
