/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2005 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_UTIL_H__
#define __LOGJAM_UTIL_H__


extern void string_replace (char **dest, char *src);

extern gboolean verify_dir (const char *path, GError **err);
extern gboolean verify_path (const char *path, gboolean include_last, GError **err);

extern void xml_escape (char **text);


#endif
