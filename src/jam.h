/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_JAM_H__
#define __LOGJAM_JAM_H__

#include "liblj/livejournal.h"

#include "jamdoc.h"
#include "jamview.h"
#include "undo.h"


typedef struct {
  GtkWindow win; /* super class */

  GtkItemFactory *factory;

  /* menu items. */
  GtkWidget *mweb, *msubmitsep, *msubmit, *msaveserver;
  GtkWidget *mundo, *mredo;

  GtkWidget *userlabel;
  GtkWidget *baction; /* "action" button: submit / save changes */
  GtkWidget *bdelete; /* "delete" button */

#if not_yet
  GtkWidget *nb_entry;/* syncbook master */
  GtkWidget *nb_meta; /* syncbook slave */
#endif
  JamDoc *doc;
  GtkWidget *view;
  JamAccount *account;

  gpointer preview;   /* we only want one preview window per jam_win. */
} JamWin;


extern void jam_font_set (GtkWidget *w, gchar *font_name);
extern void jam_run (JamDoc *doc);
extern void jam_do_changeuser (JamWin *jw);
extern gboolean jam_confirm_lose_entry (JamWin *jw);

extern void jam_clear_entry (JamWin *jw);
extern void jam_open_entry (JamWin *jw);
extern void jam_open_draft (JamWin *jw);

extern gboolean jam_save_as_file (JamWin *jw);
extern gboolean jam_save_as_draft (JamWin *jw);
extern gboolean jam_save (JamWin *jw);

extern void jam_load_entry (JamWin *jw, LJEntry *entry);
extern void jam_submit_entry (JamWin *jw);
extern void jam_save_entry_server (JamWin *jw);

extern void jam_quit (JamWin *jw);
extern void jam_autosave_init (JamWin *jw);
extern void jam_autosave_stop (JamWin *jw);

extern JamDoc *jam_win_get_cur_doc (JamWin *jw);
extern JamView *jam_win_get_cur_view (JamWin *jw);


#endif
