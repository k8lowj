/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_CONF_XML_H__
#define __LOGJAM_CONF_XML_H__

#include "conf.h"


extern int conf_read (Configuration *c, char *path);
extern int conf_write (Configuration *c, char *path);

typedef void *(*conf_parsedirxml_fn) (xmlDocPtr, xmlNodePtr, void *);
typedef void *(*conf_parsedirlist_fn) (const char *, void *);

extern void *conf_parsedirxml (const char *dirname, conf_parsedirxml_fn fn, void *data);
extern GSList *conf_parsedirlist (const char *base, conf_parsedirlist_fn fn, void *data);


#endif /* CONF_XML_H */
