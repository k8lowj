/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_SPAWN_H__
#define __LOGJAM_SPAWN_H__

#include <gtk/gtkwindow.h>

#include "conf.h"

extern const CommandList spawn_commands[];

extern void spawn_url (GtkWindow *parent, const char *url);


#endif
