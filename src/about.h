/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_ABOUT_H__
#define __LOGJAM_ABOUT_H__

#include "gtk-all.h"


extern void about_dlg (GtkWidget *mainwin);


#endif
