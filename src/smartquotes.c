/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 *
 * vim: tabstop=4 shiftwidth=4 noexpandtab :
 */
#include <stdlib.h>

#include "gtk-all.h"


static void xfree (void *p) {
  void **xp = (void **)p;
  if (*xp) free(*xp);
}
#define AUTOFREE  __attribute__((cleanup(xfree)))


#define ASCII_BACKTICK               '`'
#define ASCII_SINGLEQUOTE            '\''
#define ASCII_DOUBLEQUOTE            '"'
#define UNICODE_LEFTSINGLEQUOTE      0x2018
#define UNICODE_RIGHTSINGLEQUOTE     0x2019
#define UNICODE_LEFTDOUBLEQUOTE      0x201C
#define UNICODE_RIGHTDOUBLEQUOTE     0x201D
#define UNICODE_LEFTDOUBLEQUOTE_RU   0x00AB
#define UNICODE_RIGHTDOUBLEQUOTE_RU  0x00BB


static gboolean rusmode = FALSE;


/* return quote type:
 *   0: none
 *   1: uni-single
 *   2: uni-double
 */
static int is_quote (gunichar c) {
  if (!rusmode) {
    switch (c) {
      case ASCII_BACKTICK:
      case ASCII_SINGLEQUOTE:
      case UNICODE_LEFTSINGLEQUOTE:
      case UNICODE_RIGHTSINGLEQUOTE:
        return 1;
      case ASCII_DOUBLEQUOTE:
      case UNICODE_LEFTDOUBLEQUOTE:
      case UNICODE_RIGHTDOUBLEQUOTE:
        return 2;
      default:
        return 0;
    }
  } else {
    switch (c) {
      case ASCII_DOUBLEQUOTE:
      case UNICODE_LEFTDOUBLEQUOTE_RU:
      case UNICODE_RIGHTDOUBLEQUOTE_RU:
        return 2;
      default:
        return 0;
    }
  }
}


/* return quote type plus:
 *   3: hot char
 */
static int is_hotchar (gunichar c) {
  int qq = is_quote(c);
  if (qq) return qq;
  switch (c) {
    case '!': case '.': case '-': case ' ': case '\t': case '<': case '>': case '`': case '&': case '/':
      return 3;
    default:
      return 0;
  }
}


static void buf_replace_chars (GtkTextBuffer *buffer, GtkTextIter *pos, GtkTextIter *nextpos, gunichar oldc, gunichar newc) {
  if (oldc != newc) {
    char buf[8];
    int len;
    gtk_text_buffer_delete(buffer, pos, nextpos);
    len = g_unichar_to_utf8(newc, buf);
    buf[len] = 0;
    gtk_text_buffer_insert(buffer, pos, buf, len);
  }
}


/* only ASCII or valid UTF-8 */
static void buf_replace_char_with_string (GtkTextBuffer *buffer, GtkTextIter *pos, const char *str) {
  GtkTextIter npos = *pos;
  gtk_text_iter_forward_char(&npos);
  gtk_text_buffer_delete(buffer, pos, &npos);
  if (str != NULL && str[0]) {
    int len = strlen(str);
    gtk_text_buffer_insert(buffer, pos, str, len);
    gtk_text_iter_forward_chars(pos, len);
  }
}


/* return:
 *  -2: </code>
 *  -1: </pre>
 *   0: none
 *   1: <pre>
 *   2: <code>
 */
static int check_pre_tag (GtkTextIter pos) {
  static const char *tag_names[2] = {"pre", "code"}; /* engrish: no first char can be the same */
  int closing = 1;
  int sidx;
  gunichar c = gtk_text_iter_get_char(&pos);
  if (c != '<') return 0;
  gtk_text_iter_forward_char(&pos);
  if ((c = gtk_text_iter_get_char(&pos)) == '/') {
    closing = -1;
    gtk_text_iter_forward_char(&pos);
    c = gtk_text_iter_get_char(&pos);
  }
  gtk_text_iter_forward_char(&pos);
  if (c >= 'A' && c <= 'Z') c += 32; // convert to lower case
  for (sidx = 0; sidx < sizeof(tag_names)/sizeof(tag_names[0]); ++sidx) if (tag_names[sidx][0] == c) break;
  if (sidx < sizeof(tag_names)/sizeof(tag_names[0])) {
    // ok, try to match
    const char *s = tag_names[sidx]+1;
    while (*s) {
      c = gtk_text_iter_get_char(&pos);
      gtk_text_iter_forward_char(&pos);
      if (c >= 'A' && c <= 'Z') c += 32; // convert to lower case
      if (*s != c) return 0;
      ++s;
    }
    c = gtk_text_iter_get_char(&pos);
    //if (c == '/') return 0;
    if (c < 'A') return (sidx+1)*closing;
  }
  return 0;
}


static void run_smartquotes (GtkTextBuffer *buffer) {
  GtkTextIter pos;
  gunichar c = 0, prevca[4];
  int curnesting = -1;
  AUTOFREE int *balance = NULL; /* array */
  int balanceMax = 10;
  gboolean insidetag = FALSE, closing;
  int inside_pre_tag = 0; /* 0: none; -1: 'pre'; -2: 'code' */
  int quotes;

  /* this runs as the user is typing, so undo doesn't make much sense.
  gtk_text_buffer_begin_user_action(buffer);
  */

  if (!rusmode) {
    if ((balance = calloc(balanceMax, sizeof(int))) == NULL) return; /* shit happens... */
  }

  gtk_text_buffer_get_start_iter(buffer, &pos);
  while ((c = gtk_text_iter_get_char(&pos))) {
    /* [0],[1],[2],c,[3] */
    int tag = check_pre_tag(pos);
    GtkTextIter nextpos = pos;
    for (int f = 2; f >= 0; --f) {
      if (gtk_text_iter_get_offset(&nextpos) <= 0) {
        prevca[f] = 0;
      } else {
        gtk_text_iter_backward_char(&nextpos);
        prevca[f] = gtk_text_iter_get_char(&nextpos);
      }
    }
    /*g_printf("ofs: %d; char=%i\n", gtk_text_iter_get_offset(&pos), c);*/
    nextpos = pos;
    gtk_text_iter_forward_char(&nextpos);
    prevca[3] = gtk_text_iter_get_char(&nextpos);

    if (inside_pre_tag) {
      gtk_text_iter_forward_char(&pos);
      if (tag == inside_pre_tag) {
        inside_pre_tag = 0;
      } else {
        if (c == '<' && prevca[3] && prevca[3] != '/') {
          gtk_text_iter_backward_char(&pos);
          buf_replace_char_with_string(buffer, &pos, "&lt;");
        } else if (c == '>') {
          gtk_text_iter_backward_char(&pos);
          buf_replace_char_with_string(buffer, &pos, "&gt;");
        } else if (c == '&' && prevca[3]) {
          const char *s = NULL;
          gtk_text_iter_backward_char(&pos);
          switch (prevca[3]) {
            case 'a': s = "&amp;"; break;
            case 'l': s = "&lt;"; break;
            case 'g': s = "&gt;"; break;
          }
          if (s != NULL) {
            nextpos = pos;
            while (*s && (c = gtk_text_iter_get_char(&nextpos))) {
              gtk_text_iter_forward_char(&nextpos);
              if (*s != c) break;
              ++s;
            }
            if (!c) break; // done
            if (*s) {
              // should replace
              buf_replace_char_with_string(buffer, &pos, "&amp;");
            } else {
              // don't replace, just skip
              pos = nextpos;
            }
          } else {
            // should replace
            buf_replace_char_with_string(buffer, &pos, "&amp;");
          }
        }
      }
      continue;
    } else if (tag > 0) {
      inside_pre_tag = -tag;
      // skip tag
      gtk_text_iter_forward_char(&pos);
      while ((c = gtk_text_iter_get_char(&pos))) {
        gtk_text_iter_forward_char(&pos);
        if (c == '>') break;
      }
      continue;
    }

    /* --> */
    if (!insidetag && prevca[1] == '-' && prevca[2] == '-' && c == '>') {
      gtk_text_iter_backward_char(&pos);
      gtk_text_iter_backward_char(&pos);
      buf_replace_chars(buffer, &pos, &nextpos, c, 0x2192);
      gtk_text_iter_forward_char(&pos);
      continue;
    }

    if (c == '<') insidetag = TRUE;
    else if (c == '>') insidetag = FALSE;

    if (!insidetag) {
      /* long dash */
      if (prevca[0] == ' ' && prevca[1] == '-' && prevca[2] == '-' && c == ' ') {
        gtk_text_iter_backward_char(&pos);
        gtk_text_iter_backward_char(&pos);
        gtk_text_iter_backward_char(&nextpos);
        buf_replace_chars(buffer, &pos, &nextpos, c, 0x2014);
        gtk_text_iter_forward_char(&pos);
        continue;
      }
      /* ellipsis */
      if (prevca[1] == '.' && prevca[2] == '.' && c == '.') {
        gtk_text_iter_backward_char(&pos);
        gtk_text_iter_backward_char(&pos);
        buf_replace_chars(buffer, &pos, &nextpos, c, 0x2026);
        gtk_text_iter_forward_char(&pos);
        continue;
      }
      /* accent */
      if (prevca[2] == '`' && c == '`') {
        gtk_text_iter_backward_char(&pos);
        buf_replace_chars(buffer, &pos, &nextpos, c, 0x0301);
        gtk_text_iter_forward_char(&pos);
        continue;
      }
    }

    quotes = is_quote(c);

    if (insidetag || quotes == 0 || curnesting < -1) {
      gtk_text_iter_forward_char(&pos);
      continue;
    }

    if (rusmode) {
      closing = (curnesting >= 0);
      if (closing) {
        /*g_print("n %d right %c\n", curnesting, (char)c);*/
        buf_replace_chars(buffer, &pos, &nextpos, c, UNICODE_RIGHTDOUBLEQUOTE_RU);
        --curnesting;
      } else {
        /*g_print("n %d left %c\n", curnesting, (char)c);*/
        buf_replace_chars(buffer, &pos, &nextpos, c, UNICODE_LEFTDOUBLEQUOTE_RU);
        ++curnesting;
      }
    } else {
      closing = (curnesting >= 0 && balance[curnesting] == quotes);
      if (quotes == 1 && g_unichar_isalnum(prevca[2]) && (!closing || g_unichar_isalnum(prevca[3]))) {
        /* an apostrophe.  fix it up, but don't change nesting. */
        buf_replace_chars(buffer, &pos, &nextpos, c, UNICODE_RIGHTSINGLEQUOTE);
      } else if (closing) {
        buf_replace_chars(buffer, &pos, &nextpos, c, (quotes == 1 ? UNICODE_RIGHTSINGLEQUOTE : UNICODE_RIGHTDOUBLEQUOTE));
        --curnesting;
      } else {
        buf_replace_chars(buffer, &pos, &nextpos, c, (quotes == 1 ? UNICODE_LEFTSINGLEQUOTE : UNICODE_LEFTDOUBLEQUOTE));
        ++curnesting;
        balance[curnesting] = quotes;
      }
      if (curnesting >= balanceMax-1) {
        /*g_warning("too many nested quotes.");*/
        int newsz = balanceMax+16;
        int *newb = realloc(balance, newsz*sizeof(int));
        if (!newb) break; /* shit happens... */
        balance = newb;
        balanceMax = newsz;
        for (int f = curnesting+1; f < balanceMax; ++f) balance[f] = 0;
      }
    }
    gtk_text_iter_forward_char(&pos);
  }
  /* gtk_text_buffer_end_user_action(buffer); */
}


#define SMARTQUOTES_KEY "logjam-smartquotes-id"
static void smartquotes_begin (GtkTextBuffer *buffer);
static void smartquotes_insert_cb (GtkTextBuffer *buffer, GtkTextIter *iter, gchar *text, gint len, gpointer user_data);
static void smartquotes_delete_cb (GtkTextBuffer *buffer, GtkTextIter *i1, GtkTextIter *i2);


static gboolean smartquotes_idle_cb (GtkTextBuffer *buffer) {
  g_signal_handlers_block_by_func(buffer, smartquotes_insert_cb, SMARTQUOTES_KEY);
  g_signal_handlers_block_by_func(buffer, smartquotes_delete_cb, SMARTQUOTES_KEY);
  run_smartquotes(buffer);
  g_signal_handlers_unblock_by_func(buffer, smartquotes_insert_cb, SMARTQUOTES_KEY);
  g_signal_handlers_unblock_by_func(buffer, smartquotes_delete_cb, SMARTQUOTES_KEY);
  return FALSE;
}


static void smartquotes_begin (GtkTextBuffer *buffer) {
  GObject *obj = G_OBJECT(buffer);
  guint idleid;
  idleid = GPOINTER_TO_INT(g_object_get_data(obj, SMARTQUOTES_KEY));
  if (idleid) g_source_remove(idleid);
  idleid = g_idle_add((GSourceFunc)smartquotes_idle_cb, buffer);
  g_object_set_data(obj, SMARTQUOTES_KEY, GINT_TO_POINTER(idleid));
}


static void smartquotes_insert_cb (GtkTextBuffer *buffer, GtkTextIter *iter, gchar *text, gint len, gpointer user_data) {
  for (int i = 0; i < len; ++i) {
    if (is_hotchar(text[i])) {
      smartquotes_begin(buffer);
      break;
    }
  }
}

static void smartquotes_delete_cb(GtkTextBuffer *buffer, GtkTextIter *i1, GtkTextIter *i2) {
  gunichar c;
  GtkTextIter i = *i1;
  while (gtk_text_iter_in_range(&i, i1, i2)) {
    c = gtk_text_iter_get_char(&i);
    if (is_hotchar(c)) {
      smartquotes_begin(buffer);
      break;
    }
    gtk_text_iter_forward_char(&i);
  }
}


void smartquotes_attach (GtkTextBuffer *buffer, gboolean russian_mode) {
  rusmode = russian_mode;
  g_signal_connect(buffer, "insert-text", G_CALLBACK(smartquotes_insert_cb), SMARTQUOTES_KEY);
  g_signal_connect(buffer, "delete-range", G_CALLBACK(smartquotes_delete_cb), SMARTQUOTES_KEY);
}


void smartquotes_detach (GtkTextBuffer *buffer) {
  g_signal_handlers_disconnect_matched(G_OBJECT(buffer), G_SIGNAL_MATCH_DATA, 0, 0, NULL, NULL, SMARTQUOTES_KEY);
}
