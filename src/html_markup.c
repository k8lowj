/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2005 Evan Martin <evan@livejournal.com>
 */

#include "gtk-all.h"

#include <string.h>
#include "html_markup.h"


void html_mark_tag (JamDoc *doc, const char *tag, ...) {
  GtkTextBuffer *buffer;
  GtkTextIter start, end;
  GtkTextMark *ins;
  char *startTag, *endTag;
  int tagLen;
  va_list ap;

  if (!strcmp(tag, "span")) {
    va_start(ap, tag);
    startTag = g_strdup_printf("<%s %s>", tag, va_arg(ap, const char *));
    va_end(ap);
  } else {
    startTag = g_strdup_printf("<%s>", tag);
  }
  endTag = g_strdup_printf("</%s>", tag);
  tagLen = strlen(endTag);

  buffer = jam_doc_get_text_buffer(doc);

  gtk_text_buffer_begin_user_action(buffer);    /* start undo action */

  if (!gtk_text_buffer_get_selection_bounds(buffer, &start, &end)) {
    gtk_text_buffer_insert_at_cursor(buffer, startTag, -1);
    gtk_text_buffer_insert_at_cursor(buffer, endTag, -1);
    ins = gtk_text_buffer_get_mark(buffer, "insert");
    gtk_text_buffer_get_iter_at_mark(buffer, &start, ins);
    gtk_text_iter_backward_chars(&start, tagLen);
    gtk_text_buffer_move_mark_by_name(buffer, "insert", &start);
    gtk_text_buffer_move_mark_by_name(buffer, "selection_bound", &start);
  } else {
    gtk_text_buffer_insert(buffer, &start, startTag, -1);
    gtk_text_buffer_get_selection_bounds(buffer, &start, &end);
    gtk_text_buffer_insert(buffer, &end, endTag, -1);
    gtk_text_buffer_move_mark_by_name(buffer, "insert", &end);
    gtk_text_buffer_move_mark_by_name(buffer, "selection_bound", &end);
  }

  g_free(startTag);
  g_free(endTag);
  gtk_text_buffer_end_user_action(buffer);
}


#define MKTAG(name,...)  void html_mark_##name (JamDoc *doc) { html_mark_tag(doc, __VA_ARGS__); }
#define MKTAG2(name,t1,t2)  void html_mark_##name (JamDoc *doc) { html_mark_tag(doc, t1); html_mark_tag(doc, t2); }

MKTAG(smallcaps, "span", "style=\"font-variant:small-caps\"")

MKTAG(bold, "b")
MKTAG(italic, "i")
MKTAG(underline, "u")
MKTAG(strikeout, "s")
MKTAG(monospaced, "tt")
MKTAG(para, "p")
MKTAG(blockquote, "blockquote")
MKTAG(small, "small")
MKTAG(big, "big")
MKTAG(ulist, "ul")
MKTAG(olist, "ol")
MKTAG(listitem, "li")
MKTAG(h2, "h2")
MKTAG(h3, "h3")
MKTAG(h4, "h4")

MKTAG2(superscript, "small", "sup")
MKTAG2(subscript, "small", "sub")
