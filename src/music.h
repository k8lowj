/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_MUSIC_H__
#define __LOGJAM_MUSIC_H__

#include "conf.h"


typedef enum {
  MUSIC_SOURCE_NONE,
  MUSIC_SOURCE_XMMS,
  MUSIC_SOURCE_RHYTHMBOX,
  MUSIC_SOURCE_CUSTOM,
  MUSIC_SOURCE_COUNT
} MusicSource;


extern const CommandList music_commands[];


typedef enum {
  MUSIC_COMMAND_ERROR,
  MUSIC_COMMAND_LINE_ERROR,
  MUSIC_UNIMPLEMENTED_ERROR
} MusicError;


extern gboolean music_can_detect (GError **err);
extern char *music_detect (GError **err);

extern GQuark music_error_quark (void);
#define MUSIC_ERROR  music_error_quark()

extern MusicSource music_current_source (void);


#endif
