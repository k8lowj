/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_FRIENDS_H__
#define __LOGJAM_FRIENDS_H__

#include "gtk-all.h"

#include "account.h"


extern void friends_manager_show (GtkWindow *mainwin, JamAccountLJ *acc);


#endif
