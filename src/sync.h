/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_SYNC_H__
#define __LOGJAM_SYNC_H__


extern gboolean sync_run (JamAccountLJ *acc, gpointer parent);


#endif
