/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_THROBBER_H__
#define __LOGJAM_THROBBER_H__


#define THROBBER(obj)  (G_TYPE_CHECK_INSTANCE_CAST((obj), throbber_get_type(), Throbber))

typedef struct _Throbber Throbber;

extern GType throbber_get_type (void);
extern GtkWidget *throbber_new (void);

extern void throbber_start (Throbber *t);
extern void throbber_stop (Throbber *t);
extern void throbber_reset (Throbber *t);


#endif
