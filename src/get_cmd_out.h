/* logjam - a GTK client for LiveJournal.
 *
 * Functions to get the output of external command.
 * Copyright (C) 2004, Kir Kolyshkin <kir@sacred.ru>
 */
#ifndef __LOGJAM_GET_CMD_OUT_H__
#define __LOGJAM_GET_CMD_OUT_H__


extern GString *get_command_output (const char *command, GError **err, GtkWindow *parent);


#endif
