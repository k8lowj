/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2005 Evan Martin <martine@danga.com>
 */
#ifndef __LOGJAM_PROGRESS_H__
#define __LOGJAM_PROGRESS_H__


#define PROGRESS_WINDOW(obj)  (G_TYPE_CHECK_INSTANCE_CAST((obj), progress_window_get_type(), ProgressWindow))

typedef struct _ProgressWindow ProgressWindow;

typedef void (*ProgressWindowCancelFunc) (gpointer data);

extern GType progress_window_get_type (void);
extern GtkWidget *progress_window_new (GtkWindow *parent, const char *title);

extern void progress_window_set_title (ProgressWindow *pw, const char *title);
extern void progress_window_set_text (ProgressWindow *pw, const char *text);
extern void progress_window_pack (ProgressWindow *pw, GtkWidget *contents);
extern void progress_window_show_error (ProgressWindow *pw, const char *fmt, ...) __attribute__((format(printf,2,3)));
extern void progress_window_set_progress (ProgressWindow *pw, float frac);
extern void progress_window_set_cancel_cb (ProgressWindow *pw, ProgressWindowCancelFunc func, gpointer data);


#endif
