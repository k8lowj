/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_SMARTQUOTES_H__
#define __LOGJAM_SMARTQUOTES_H__


extern void smartquotes_attach (GtkTextBuffer *buffer, gboolean russian_mode);
extern void smartquotes_detach (GtkTextBuffer *buffer);


#endif
