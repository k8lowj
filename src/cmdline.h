/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2004 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_CMDLINE_H__
#define __LOGJAM_CMDLINE_H__

#include "jamdoc.h"


extern void cmdline_parse (JamDoc *doc, int argc, char *argv[]);


#endif
