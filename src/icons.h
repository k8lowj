/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_ICONS_H__
#define __LOGJAM_ICONS_H__


extern void icons_initialize (void);
extern GdkPixbuf *icons_rarrow_pixbuf (void);
extern GdkPixbuf *icons_larrow_pixbuf (void);
extern GdkPixbuf *icons_lrarrow_pixbuf (void);

#ifndef HAVE_LIBRSVG
/* how many throbber images we have... */
#define THROBBER_COUNT  (8)
extern void icons_load_throbber (GdkPixbuf *pbs[]);
#endif /* HAVE_LIBRSVG */


#endif
