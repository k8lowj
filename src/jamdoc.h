/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_JAMDOC_H__
#define __LOGJAM_JAMDOC_H__

#include "liblj/livejournal.h"

#include "account.h"


typedef enum {
  ENTRY_DRAFT,
  ENTRY_NEW,
  ENTRY_SERVER
} LJEntryType;


typedef struct _JamDoc JamDoc;
typedef struct _JamDocClass JamDocClass;


extern GType jam_doc_get_type(void);
extern JamDoc *jam_doc_new(void);

#ifdef HAVE_GTK
#include <gtk/gtktextbuffer.h>
extern GtkTextBuffer *jam_doc_get_text_buffer (JamDoc *doc);
extern gboolean jam_doc_append_text (JamDoc *doc, const char *text, const char *encoding);
extern gboolean jam_doc_insert_file (JamDoc *doc, const char *filename, const char *encoding, GError **err);
extern gboolean jam_doc_insert_command_output (JamDoc *doc, const char *command, const char *encoding, GError **err, GtkWindow *parent);
#endif

extern const char *jam_doc_get_location (JamDoc *doc);
extern void jam_doc_set_location (JamDoc *doc, const char *location);

extern char *jam_doc_get_title (JamDoc *doc);
extern LJEntry *jam_doc_get_entry (JamDoc *doc);

extern char *jam_doc_get_draftname (JamDoc *doc);

/* these flags correspond to server-side capabilities;
 * any document can be saved with a file->save as sort of command. */
#define LOGJAM_DOC_CAN_DELETE  (1<<0)
#define LOGJAM_DOC_CAN_SAVE    (1<<1)
#define LOGJAM_DOC_CAN_SUBMIT  (1<<2)

extern int jam_doc_get_flags (JamDoc *doc);

extern void jam_doc_set_dirty (JamDoc *doc, gboolean dirty);
extern gboolean jam_doc_get_dirty (JamDoc *doc);

extern JamAccount *jam_doc_get_account (JamDoc *doc);
extern void jam_doc_set_account (JamDoc *doc, JamAccount *acc);

extern const char *jam_doc_get_subject (JamDoc *doc);
extern void jam_doc_set_subject (JamDoc *doc, const char *subject);

extern LJSecurity jam_doc_get_security (JamDoc *doc);
extern void jam_doc_set_security (JamDoc *doc, LJSecurity *sec);

extern const char *jam_doc_get_mood (JamDoc *doc);
extern void jam_doc_set_mood (JamDoc *doc, const char *mood);

extern void jam_doc_set_moodid (JamDoc *doc, int moodid);

extern const char *jam_doc_get_music (JamDoc *doc);
extern void jam_doc_set_music (JamDoc *doc, const char *music);

extern const char *jam_doc_get_taglist (JamDoc *doc);
extern void jam_doc_set_taglist (JamDoc *doc, const char *taglist);

extern const char *jam_doc_get_picture (JamDoc *doc);
extern void jam_doc_set_picture (JamDoc *doc, const char *keyword);

extern LJCommentsType jam_doc_get_comments (JamDoc *doc);
extern void jam_doc_set_comments (JamDoc *doc, LJCommentsType type);

extern LJScreeningType jam_doc_get_screening (JamDoc *doc);
extern void jam_doc_set_screening (JamDoc *doc, LJScreeningType type);

extern void jam_doc_get_time (JamDoc *doc, struct tm *ptm);
extern void jam_doc_set_time (JamDoc *doc, const struct tm *ptm);

extern gboolean jam_doc_get_backdated (JamDoc *doc);
extern void jam_doc_set_backdated (JamDoc *doc, gboolean backdated);

extern gboolean jam_doc_get_preformatted (JamDoc *doc);
extern void jam_doc_set_preformatted (JamDoc *doc, gboolean preformatted);

extern void jam_doc_set_event (JamDoc *doc, const char *event);

extern gchar *jam_doc_get_usejournal (JamDoc *doc);
extern void jam_doc_set_usejournal (JamDoc *doc, const gchar *usejournal);

extern gboolean jam_doc_load_file (JamDoc *doc, const char *filename, LJEntryFileType type, GError **err);
extern void jam_doc_load_draft (JamDoc *doc, LJEntry *entry);
extern void jam_doc_load_entry (JamDoc *doc, LJEntry *entry);

/* these are terrible names. */
extern gboolean jam_doc_would_save_over_nonxml (JamDoc *doc);
extern gboolean jam_doc_has_save_target (JamDoc *doc);

extern gboolean jam_doc_save (JamDoc *doc, JamAccount *acc, GError **err);

/* these emit the "title-changed" signal */
extern gboolean jam_doc_save_as_file (JamDoc *doc, const char *filename, GError **err);
extern gboolean jam_doc_save_as_draft (JamDoc *doc, const char *title, JamAccount *acc, GError **err);


extern LJEntryType jam_doc_get_entry_type (JamDoc *doc);
extern gint jam_doc_get_entry_itemid (JamDoc *doc);

extern const char *jam_doc_get_url (JamDoc *doc);
extern void jam_doc_set_url (JamDoc *doc, const char *url);
extern void jam_doc_reset_url (JamDoc *doc);

#define LOGJAM_TYPE_DOC          (jam_doc_get_type())
#define LOGJAM_DOC(object)       (G_TYPE_CHECK_INSTANCE_CAST((object), LOGJAM_TYPE_DOC, JamDoc))
#define LOGJAM_DOC_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST ((klass), LOGJAM_TYPE_DOC, JamDocClass))


#endif
