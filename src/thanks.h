typedef struct {
  char *name;
  char *username;
  char *email;
  char *contribution;
} Contributor;

Contributor contribs[] = {
  { "Aleksander Adamowski", "", "aleksander.adamowski@gmail.com", N_("patches") },
  { "Satoru SATOH", "", "ss@gnome.gr.jp", N_("Japanese translation") },
  { "Grigory Bakunov", "", "black@asplinux.ru", N_("Russian translation") },
  { "Frank Hofmann", "", "frankhofmann@gmx.ch", N_("German translation") },
  { "George Mason", "", "georgemason@mindspring.com", N_("accelerator saving") },
  { "Dave Spadea", "", "dave@spadea.net", N_("lj-cut patch") },
  { "Dmitry Dyomin", "old_one", "old@old.com.ua", N_("Ukrainian translation") },
  { "Ricardo Mones Lastra", "mones", "ricardo.mones@hispalinux.es", N_("Spanish translation") },
  { "Jon Nall", "", "nall@gentoo.org", N_("Gentoo packaging") },
  { "Mikhail Chekanov", "", "mihun@bitex.com", N_("Russian translation") },
  { "Asaf Bartov", "ijon", "ijon@forum2.org", N_("win32 multithread support") },
  { "Gaal Yahas", "gaal", "gaal@forum2.org", N_("checkfriends, poll creator, i18n") },
  { "Sergei Barbarash", "sgt", "sgt@fep.ru", N_("support for cyrillic translation") },
  { "Michael Bravo", "", "mbravo@tag-ltd.spb.ru", N_("locale fixes") },
  { "Ari Pollak", "", "ari@debian.org", N_("Debian package, undo") },
  { "Steve Bernacki, Jr.", "", "steve@copacetic.net", N_("typo in configure.in") },
  { "Artem Baguinski", "", "artm@v2.nl", N_("locale setting") },
  { "nulld", "", "nulld@encryptedemail.net", N_("OpenBSD box to test on") },
  { "Decklin Foster", "decklin", "decklin@red-bean.com", N_("window geometry save/restore") },
  { "Matthew Vernon", "", "matthew@pick.ucam.org", N_("simple login proxy patch") },
  { "Bill Crawford", "", "bill@eb0ne.net", N_("variety of patches") },
  { "Gabriel Martinez", "diamondc", "gabrielfm@yahoo.com", N_("Solaris box to test on") },
  { "Tom Callaway", "spot", "spot@redhat.com", N_("Advanced RPM packaging") },
  { "Alexander Gräfe", "", "nachtfalke@retrogra.de", N_("Initial RPM packaging") },
  { "Christian Surchi", "christian", "csurchi@debian.org", N_("Debian maintainer") },
  { "Joe Wreschnig", "piman", "piman@sacredchao.net", N_("Debian package help") },
  { "Ryan Younce", "", "ryan@manunkind.org", N_("FreeBSD packaging") },
  { "Raja Mukerji", "", "mukerji@pobox.com", N_("a multitude of patches") },
  { "Adam Bidema", "", "hijinx@klone.nwraves.net", N_("initial file save patch") },
  { "Matt Roper", "", "mdroper@ucdavis.edu", N_("parallel make tips") },
  { "Brad Fitzpatrick", "", "brad@danga.com", N_("much of the original client") },
};

#define CONTRIBCOUNT 31
