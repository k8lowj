/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#include "gtk-all.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "liblj/getevents.h"

#include "conf.h"
#include "history.h"
#include "network.h"
#include "spawn.h"


LJEntry *history_load_itemid (GtkWindow *parent, JamAccount *acc, const char *usejournal, int itemid) {
  NetContext *ctx;
  LJGetEventsSingle *getevents;
  LJEntry *entry;

  if (!JAM_ACCOUNT_IS_LJ(acc)) {
    g_warning("XXX blogger: history for blogger\n");
    return NULL;
  }

  getevents = lj_getevents_single_new(jam_account_lj_get_user(JAM_ACCOUNT_LJ(acc)), usejournal, itemid);
  ctx = net_ctx_gtk_new(parent, _("Loading Entry"));
  if (!net_run_verb_ctx((LJVerb *) getevents, ctx, NULL)) {
    lj_getevents_single_free(getevents, TRUE);
    net_ctx_gtk_free(ctx);
    return NULL;
  }
  entry = getevents->entry;
  lj_getevents_single_free(getevents, FALSE);
  net_ctx_gtk_free(ctx);

  return entry;
}


LJEntry *history_load_latest (GtkWindow *win, JamAccount *acc, const char *usejournal) {
  return history_load_itemid(win, acc, usejournal, -1);
}
