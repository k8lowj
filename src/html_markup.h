/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2005 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_HTML_MARKUP_H__
#define __LOGJAM_HTML_MARKUP_H__

#include "jamdoc.h"


extern void html_mark_tag (JamDoc *doc, const char *tag, ...);

extern void html_mark_bold (JamDoc *doc);
extern void html_mark_italic (JamDoc *doc);
extern void html_mark_underline (JamDoc *doc);
extern void html_mark_strikeout (JamDoc *doc);
extern void html_mark_monospaced (JamDoc *doc);
extern void html_mark_para (JamDoc *doc);
extern void html_mark_smallcaps (JamDoc *doc);
extern void html_mark_blockquote (JamDoc *doc);
extern void html_mark_big (JamDoc *doc);
extern void html_mark_small (JamDoc *doc);
extern void html_mark_superscript (JamDoc *doc);
extern void html_mark_subscript (JamDoc *doc);
extern void html_mark_ulist (JamDoc *doc);
extern void html_mark_olist (JamDoc *doc);
extern void html_mark_listitem (JamDoc *doc);
extern void html_mark_h2 (JamDoc *doc);
extern void html_mark_h3 (JamDoc *doc);
extern void html_mark_h4 (JamDoc *doc);


#endif
