/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_REMOTE_H__
#define __LOGJAM_REMOTE_H__


extern GQuark remote_error_quark (void);
#define REMOTE_ERROR  remote_error_quark()


typedef enum {
  REMOTE_ERROR_SYSTEM
} RemoteError;


typedef struct _LogJamRemote LogJamRemote;
typedef struct _LogJamRemoteClass LogJamRemoteClass;


extern LogJamRemote *logjam_remote_new (void);

extern gboolean logjam_remote_is_listening (LogJamRemote *remote);

extern gboolean logjam_remote_listen (LogJamRemote *remote, GError **err);
extern gboolean logjam_remote_stop_listening (LogJamRemote *remote, GError **err);
extern gboolean logjam_remote_send_present (LogJamRemote *remote, GError **err);

/* sending side doesn't need an object. */
extern gboolean remote_send_user (const char *username, GError **err);


#endif
