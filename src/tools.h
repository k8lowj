/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_TOOLS_H__
#define __LOGJAM_TOOLS_H__

#include "jamdoc.h"


extern void tools_html_escape (GtkWindow *win, JamDoc *doc);
extern void tools_remove_linebreaks (GtkWindow *win, JamDoc *doc);
extern void tools_insert_file (GtkWindow *win, JamDoc *doc);
extern void tools_insert_command_output (GtkWindow *win, JamDoc *doc);
extern void tools_validate_xml (GtkWindow *win, JamDoc *doc);
extern void tools_ljcut (GtkWindow *win, JamDoc *doc);


#endif
