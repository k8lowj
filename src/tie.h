/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_TIE_H__
#define __LOGJAM_TIE_H__


extern GtkWidget *tie_toggle (GtkToggleButton *toggle, gboolean *data);
extern void tie_text (GtkEntry *entry, char **data);
extern void tie_combo (GtkCombo *combo, char **data);


#endif
