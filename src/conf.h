/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_CONF_H__
#define __LOGJAM_CONF_H__

#include "liblj/livejournal.h"

#ifdef HAVE_GTK
# include "gtk-all.h"
# include "jamview.h" /* need META_COUNT */
#endif

#include "account.h"
#include "checkfriends.h"


typedef struct {
  int x, y, width, height;
  int panedpos; /* optional; for windows with panes. */
} Geometry;


/* this should match the geometry_names[] array in conf_xml.c */
typedef enum {
  GEOM_MAIN,
  GEOM_LOGIN,
  GEOM_FRIENDS,
  GEOM_FRIENDGROUPS,
  GEOM_CONSOLE,
  GEOM_MANAGER,
  GEOM_CFFLOAT,
  GEOM_OFFLINE,
  GEOM_PREVIEW,
  GEOM_COUNT
} GeometryType;


typedef struct {
  gboolean netdump;
  gboolean nofork;
  gboolean useproxy;
  gboolean useproxyauth;
#ifdef HAVE_GTK
# ifdef HAVE_GTKSPELL
  gboolean usespellcheck;
# endif
  gboolean revertusejournal;
  gboolean autosave;
  gboolean cfautostart;
  gboolean cfusemask;
  gboolean docklet;
  gboolean cffloat;
  gboolean cffloatraise;
  gboolean cffloat_decorate;
  gboolean friends_hidestats;
  gboolean allowmultipleinstances;
  gboolean smartquotes;
  gboolean smartquotes_russian;
  gboolean showmeta[JAM_VIEW_META_COUNT];
#endif /* HAVE_GTK */
} Options;


enum {
  POSTMODE_GUI,
  POSTMODE_CMD
} PostmodeType;


typedef struct {
  const char *label;
  const char *command;
} CommandList;


typedef struct {
  /* configuration file */
  GSList *hosts;
  JamHost *lasthost;

  Geometry geometries[GEOM_COUNT];

  Options options;

  gchar *uifont;
#ifdef HAVE_GTKSPELL
  gchar *spell_language;
#endif
  char *spawn_command;

  char *music_command;

  char *proxy;
  char *proxyuser, *proxypass;

  LJSecurity defaultsecurity;

  gint cfuserinterval;
  gint cfthreshold;

  /* run-time settings. */
  int postmode;
} Configuration;


typedef struct {
  gchar *programname;
  gchar *conf_dir; /* may be null, which means <home>/.logjam/ */

  gboolean cli; /* true if there's no gui */
  gboolean quiet;

#ifdef HAVE_GTK
  GtkTooltips *tooltips;
  GSList *secmgr_list;

  GSList *quiet_dlgs;

  CFMgr *cfmgr;
  CFFloat *cf_float;

  gint autosave; /* timeout id */

  void *remote;
  void *docklet;
#endif
} Application;


extern Configuration conf;
extern Application app;


extern JamHost *conf_host_by_name (Configuration *c, const char *hostname);

extern int conf_verify_dir (void);
extern void conf_make_path (const char *file, char *buf, size_t bufsz);

extern char *conf_make_account_path (JamAccount *acc, const char *path);

extern void conf_verify_a_host_exists (void);

extern gboolean conf_rename_host (JamHost *host, const char *newname, GError **err);


#endif /* CONF_H */
