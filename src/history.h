/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_HISTORY_H__
#define __LOGJAM_HISTORY_H__

#include "account.h"


extern LJEntry *history_recent_dialog_run (GtkWindow *parent, JamAccount *acc, const char *usejournal);
extern LJEntry *history_load_latest (GtkWindow *parent, JamAccount *acc, const char *usejournal);
extern LJEntry *history_load_itemid (GtkWindow *parent, JamAccount *acc, const char *usejournal, int itemid);


#endif
