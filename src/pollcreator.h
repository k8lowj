/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_POLLCREATOR_H__
#define __LOGJAM_POLLCREATOR_H__


extern void run_poll_creator_dlg (JamWin * jw);


#endif
