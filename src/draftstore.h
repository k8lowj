/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_DRAFTSTORE_H__
#define __LOGJAM_DRAFTSTORE_H__

#include "account.h"

#include "liblj/livejournal.h"


typedef struct _DraftStore DraftStore;

typedef void (*DraftStoreHeaderFunc) (DraftStore *, LJEntry *, gpointer);


extern DraftStore *draft_store_new (JamAccount *acc);
extern void draft_store_free (DraftStore *ds);

extern gboolean draft_store_each_header (DraftStore *ds, LJEntry *entry, DraftStoreHeaderFunc func, gpointer data);

extern LJEntry *draft_store_get_entry (DraftStore *ds, int itemid, GError **err);
extern gboolean draft_store_put_entry (DraftStore *ds, LJEntry *entry, GError **err);
extern gboolean draft_store_remove_entry (DraftStore *ds, int itemid, GError **err);
extern int draft_store_find_itemid (DraftStore *ds);
extern gboolean draft_store_flush (DraftStore *ds, GError **err);

#ifdef HAVE_GTK
extern LJEntry *draft_store_ui_select (DraftStore *ds, GtkWindow *parent);
#endif


#endif /* DRAFTSTORE_H */
