/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2004 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_USEJOURNAL_H__
#define __LOGJAM_USEJOURNAL_H__


extern GtkWidget *usejournal_build_menu (const char *defaultjournal, const char *currentjournal, GSList *journals, gpointer doc);


#endif
