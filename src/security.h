/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_SECURITY_H__
#define __LOGJAM_SECURITY_H__

#include <gtk/gtkoptionmenu.h>

#include "liblj/livejournal.h"

#include "account.h"


/* A SecMgr is a widget which manages a security setting.
 * The LJSecurity object now lives in livejournal.[ch].
 */

#define SECMGR(obj)  (G_TYPE_CHECK_INSTANCE_CAST((obj), secmgr_get_type(), SecMgr))

typedef struct _SecMgr SecMgr;


extern GType secmgr_get_type (void);
extern GtkWidget *secmgr_new (gboolean withcustom);
extern void secmgr_security_set (SecMgr *secmgr, const LJSecurity *security);
extern void secmgr_security_set_force (SecMgr *secmgr, const LJSecurity *security);
extern void secmgr_security_get (SecMgr *secmgr, LJSecurity *security);
extern void secmgr_set_account (SecMgr *sm, JamAccountLJ *account);

extern guint32 custom_security_dlg_run (GtkWindow *parent, guint32 mask, JamAccountLJ *acc);


#endif
