/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Gaal Yahas <gaal@forum2.org>
 */
#ifndef __LOGJAM_ACCOUNT_H__
#define __LOGJAM_ACCOUNT_H__

#include <glib.h>
#include <glib-object.h>
#include <libxml/tree.h>

#include "liblj/livejournal.h"

/* need netcontext struct */
#include "network.h"


#define JAM_TYPE_ACCOUNT            (jam_account_get_type())
#define JAM_ACCOUNT(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), JAM_TYPE_ACCOUNT, JamAccount))
#define JAM_ACCOUNT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), JAM_TYPE_ACCOUNT, JamAccountClass))


typedef struct _JamAccountClass JamAccountClass;
typedef struct _JamAccount JamAccount;


#define JAM_TYPE_HOST            (jam_host_get_type())
#define JAM_HOST(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj), JAM_TYPE_HOST, JamHost))
#define JAM_HOST_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), JAM_TYPE_HOST, JamHostClass))


typedef struct _JamHostClass JamHostClass;
typedef struct _JamHost JamHost;


struct _JamAccount {
  GObject obj;

  JamHost *host;

  gboolean remember_user;
  gboolean remember_password;
};


struct _JamAccountClass {
  GObjectClass parent_class;

  void (*set_username) (JamAccount *acc, const char *username);
  const gchar *(*get_username) (JamAccount *acc);
  void (*set_password) (JamAccount *acc, const char *password);
  const gchar *(*get_password) (JamAccount *acc);
};


typedef struct _JamAccountLJ JamAccountLJ;


#define JAM_TYPE_ACCOUNT_LJ     (jam_account_lj_get_type())
#define JAM_ACCOUNT_LJ(obj)     (G_TYPE_CHECK_INSTANCE_CAST((obj), JAM_TYPE_ACCOUNT_LJ, JamAccountLJ))
#define JAM_ACCOUNT_IS_LJ(obj)  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAM_TYPE_ACCOUNT_LJ))


struct _JamAccountLJ {
  JamAccount account;
  LJUser *user;
  guint32 cfmask;
  time_t lastupdate;
};


typedef struct _JamHostLJ JamHostLJ;

#define JAM_TYPE_HOST_LJ     (jam_host_lj_get_type())
#define JAM_HOST_LJ(obj)     (G_TYPE_CHECK_INSTANCE_CAST((obj), JAM_TYPE_HOST_LJ, JamHostLJ))
#define JAM_HOST_IS_LJ(obj)  (G_TYPE_CHECK_INSTANCE_TYPE ((obj), JAM_TYPE_HOST_LJ))


struct _JamHost {
  GObject obj;

  char *name;
  GSList *accounts;
  JamAccount *lastaccount;
};


struct _JamHostClass {
  GObjectClass parent_class;

  const char *(*get_stock_icon) (void);
  void (*save_xml) (JamHost *host, xmlNodePtr servernode);
  gboolean (*load_xml) (JamHost *host, xmlDocPtr doc, xmlNodePtr servernode);
  JamAccount *(*make_account) (JamHost *host, const char *username);

  gboolean (*do_post) (JamHost *host, NetContext *ctx, void *doc, GError **err);
  gboolean (*do_edit) (JamHost *host, NetContext *ctx, void *doc, GError **err);
  gboolean (*do_delete) (JamHost *host, NetContext *ctx, void *doc, GError **err);
};


extern void jam_account_logjam_init (void);
extern GType jam_account_get_type (void);
extern JamAccount *jam_account_from_xml (xmlDocPtr doc, xmlNodePtr node, JamHost *host);
extern JamAccount *jam_account_make (LJUser *u);
extern JamAccount *jam_account_new_from_names (const gchar *username, const gchar *servername);
extern gchar *jam_account_id_strdup_from_names (const gchar *username, const gchar *servername);
extern gchar *jam_account_id_strdup (JamAccount *acc);
extern JamAccount *jam_account_lookup (gchar *id);
extern JamAccount *jam_account_lookup_by_user (LJUser *u);

extern void jam_account_connect (JamAccount *acc, gboolean connect);
extern void jam_account_connect_all (gboolean connect);

extern const gchar *jam_account_get_username (JamAccount *acc);
extern const gchar *jam_account_get_password (JamAccount *acc);
extern JamHost *jam_account_get_host (JamAccount *acc);

extern void jam_account_set_username (JamAccount *acc, const char *username);
extern void jam_account_set_password (JamAccount *acc, const char *password);
extern void jam_account_set_remember (JamAccount *acc, gboolean u, gboolean p);
extern void jam_account_get_remember (JamAccount *acc, gboolean *u, gboolean *p);
extern gboolean jam_account_get_remember_password (JamAccount *acc);

extern gboolean jam_account_write (JamAccount *account, GError **err);

extern GType jam_account_lj_get_type (void);
extern JamAccount *jam_account_lj_new (LJServer *server, const char *username);
extern JamAccount *jam_account_lj_from_xml(xmlDocPtr doc, xmlNodePtr node, JamHostLJ * host);
extern void jam_account_lj_write (JamAccountLJ *account, xmlNodePtr node);

extern LJUser *jam_account_lj_get_user (JamAccountLJ *acc);
extern LJServer *jam_account_lj_get_server (JamAccountLJ *acc);
extern guint32 jam_account_lj_get_cfmask (JamAccountLJ *acc);
extern void jam_account_lj_set_cfmask (JamAccountLJ *acc, guint32 mask);
extern gboolean jam_account_lj_get_checkfriends (JamAccount *acc);


typedef enum {
  JAM_HOST_SUBMITTING_ENTRY,
  JAM_HOST_SAVING_CHANGES,
  JAM_HOST_DELETING_ENTRY,
} JamHostActionTitle;


extern GType jam_host_get_type (void);
extern const char *jam_host_get_stock_icon (JamHost *host);

extern JamAccount *jam_host_get_account_by_username (JamHost *host, const char *username, gboolean create);
extern void jam_host_add_account (JamHost *host, JamAccount *acc);

extern JamHost *jam_host_from_xml (xmlDocPtr doc, xmlNodePtr node, void *data);
extern gboolean jam_host_write(JamHost *host, GError **err);

extern gboolean jam_host_do_post (JamHost *host, NetContext *ctx, void *doc, GError **err);
extern gboolean jam_host_do_edit (JamHost *host, NetContext *ctx, void *doc, GError **err);
extern gboolean jam_host_do_delete (JamHost *host, NetContext *ctx, void *doc, GError **err);


extern LJServer *jam_host_lj_get_server (JamHostLJ *host);

extern JamHostLJ *jam_host_lj_new (LJServer *s);
extern GType jam_host_lj_get_type (void);


#endif
