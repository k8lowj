/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_FRIENDGROUPEDIT_H__
#define __LOGJAM_FRIENDGROUPEDIT_H__

#include "gtk-all.h"

#include "liblj/friends.h"


extern LJFriendGroup *friend_group_edit_dlg_run (GtkWindow *parent, JamAccountLJ *acc, LJFriendGroup *fg, int freegroup);


#endif
