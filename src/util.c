/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#include "glib-all.h"

#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sys/stat.h>
#include <sys/types.h>

#include "util.h"


void string_replace (char **dest, char *src) {
  if (*dest) g_free(*dest);
  *dest = src;
}


gboolean verify_dir (const char *path, GError **err) {
  /* mode 0700 so other people can't peek at passwords! */
  if (mkdir(path, 0700) < 0 && errno != EEXIST) {
    g_set_error(err, 0, 0, /* FIXME domain */ _("Failed to create directory '%s': %s"), path, g_strerror(errno));
    return FALSE;
  }
  return TRUE;
}


static gboolean verify_path_ex (char *path, int include_last, GError **err) {
  int i, len, reallen;
  len = reallen = (int)strlen(path);
  if (!include_last) {
    for (i = len-1; i > 0; --i) if (path[i] == G_DIR_SEPARATOR) break;
    if (i > 0) { len = i; path[len] = 0; }
  }
  /* the common case is that the path already exists */
  if (!verify_dir(path, NULL)) {
    /* otherwise, start creating parent directories until we succeed */
    for (i = len-1; i > 0; --i) {
      if (path[i] == G_DIR_SEPARATOR) {
        path[i] = 0;
        if (verify_dir(path, NULL)) { path[i] = G_DIR_SEPARATOR; break; }
        path[i] = G_DIR_SEPARATOR;
      }
    }
    /* once a parent dir succeeded, create the subdirectories we needed */
    ++i;
    for (; i < len; ++i) {
      if (path[i] == G_DIR_SEPARATOR) {
        path[i] = 0;
        if (!verify_dir(path, err)) return FALSE;
        path[i] = G_DIR_SEPARATOR;
      }
    }
    if (!verify_dir(path, err)) return FALSE;
  }
  if (!include_last) path[len] = G_DIR_SEPARATOR;
  return TRUE;
}


static void _xfree_ptr (void *p) {
  void **pp = (void **)p;
  if (*pp) free(*pp);
}


gboolean verify_path (const char *path, int include_last, GError **err) {
  if (path != NULL) {
    __attribute__((cleanup(_xfree_ptr))) char *p = strdup(path);
    return verify_path_ex(p, include_last, err);
  }
  return FALSE;
}


void xml_escape (char **text) {
  char *esc;
  if (!text || !*text) return;
  esc = g_markup_escape_text(*text, -1);
  g_free(*text);
  *text = esc;
}
