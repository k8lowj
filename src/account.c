/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Gaal Yahas <gaal@forum2.org>
 */
#include <string.h>

#include "account.h"
#include "conf.h"
#include "conf_xml.h"
#include "jam_xml.h"
#include "util.h"


static GObjectClass *jam_account_parent_class = NULL;
static GHashTable *jam_account_cache = NULL;


void jam_account_logjam_init (void) {
  /* can't be in jam_account_class_init because we can call
   * jam_account_lookup before the jam_account type exists! */
  jam_account_cache = g_hash_table_new(g_str_hash, g_str_equal);
}


static void jam_account_finalize (GObject *object) {
  JamAccount *acc = JAM_ACCOUNT(object);
  /* unregister it */
  g_hash_table_remove(jam_account_cache, acc);
  /* must chain up */
  (*jam_account_parent_class->finalize)(object);
}


static void jam_account_class_init (GObjectClass *class) {
  jam_account_parent_class = g_type_class_peek_parent(class);
  class->finalize = jam_account_finalize;
}


const gchar *jam_account_get_username (JamAccount *acc) {
  return JAM_ACCOUNT_GET_CLASS(acc)->get_username(acc);
}


const gchar *jam_account_get_password (JamAccount *acc) {
  return JAM_ACCOUNT_GET_CLASS(acc)->get_password(acc);
}


void jam_account_set_username (JamAccount *acc, const char *username) {
  JAM_ACCOUNT_GET_CLASS(acc)->set_username(acc, username);
}


void jam_account_set_password (JamAccount *acc, const char *password) {
  JAM_ACCOUNT_GET_CLASS(acc)->set_password(acc, password);
}


JamHost *jam_account_get_host (JamAccount *acc) {
  return acc->host;
}


void jam_account_set_remember (JamAccount *acc, gboolean u, gboolean p) {
  acc->remember_user = u;
  acc->remember_password = p;
}


void jam_account_get_remember (JamAccount *acc, gboolean *u, gboolean *p) {
  if (u) *u = acc->remember_user;
  if (p) *p = acc->remember_password;
}


gboolean jam_account_get_remember_password (JamAccount *acc) {
  return acc->remember_password;
}


gchar *jam_account_id_strdup_from_names (const gchar *username, const gchar *hostname) {
  return g_strdup_printf("%s@%s", username, hostname);
}


gchar *jam_account_id_strdup (JamAccount *acc) {
  return jam_account_id_strdup_from_names(jam_account_get_username(acc), acc->host->name);
}


JamAccount *jam_account_lookup(gchar *id) {
  return (JamAccount *)g_hash_table_lookup(jam_account_cache, id);
}


void jam_account_rename (JamAccount *acc, const gchar *username, const gchar *hostname) {
  gchar *newid = jam_account_id_strdup_from_names(username, hostname);
  gchar *oldid = jam_account_id_strdup(acc);
  if (!g_hash_table_steal(jam_account_cache, oldid)) g_error("%s", "can't rename account: old account with this name not found");
  jam_account_set_username(acc, username);
  string_replace(&(acc->host->name), (gchar *)hostname);
  g_free(oldid);
  g_hash_table_insert(jam_account_cache, newid, acc);
}


GType jam_account_get_type (void) {
  static GType new_type = 0;
  if (!new_type) {
    const GTypeInfo new_info = {
      sizeof(JamAccountClass),
      NULL,
      NULL,
      (GClassInitFunc) jam_account_class_init,
      NULL,
      NULL,
      sizeof(JamAccount),
      0,
      NULL
    };
    new_type = g_type_register_static(G_TYPE_OBJECT, "JamAccount", &new_info, G_TYPE_FLAG_ABSTRACT);
  }
  return new_type;
}


JamAccount *jam_account_from_xml (xmlDocPtr doc, xmlNodePtr node, JamHost *host) {
  JamAccount *acc;
  xmlChar *protocol = xmlGetProp(node, BAD_CAST "protocol");
  if (!protocol || xmlStrcmp(protocol, BAD_CAST "livejournal") == 0) {
    acc = jam_account_lj_from_xml(doc, node, JAM_HOST_LJ(host));
  } else {
    g_error("unknown protocol '%s'\n", protocol);
    return NULL;
  }
  if (protocol) xmlFree(protocol);
  acc->remember_user = TRUE;
  if (jam_account_get_password(acc)) acc->remember_password = TRUE;
  acc->host = host;
  return acc;
}


gboolean jam_account_write (JamAccount *account, GError **err) {
  xmlDocPtr doc = NULL;
  char *path;
  xmlNodePtr node;
  gboolean ret = FALSE;

  if (!account->remember_user) return TRUE;
  path = g_build_filename(app.conf_dir, "servers", account->host->name, "users", jam_account_get_username(account), "conf.xml", NULL);
  if (!verify_path(path, FALSE, err)) goto out;
  jam_xmlNewDoc(&doc, &node, "user");
  xmlNewTextChild(node, NULL, BAD_CAST "username", BAD_CAST jam_account_get_username(account));
  if (account->remember_password) xmlNewTextChild(node, NULL, BAD_CAST "password", BAD_CAST jam_account_get_password(account));
  if (JAM_ACCOUNT_IS_LJ(account)) {
    jam_account_lj_write(JAM_ACCOUNT_LJ(account), node);
    xmlSetProp(node, BAD_CAST "protocol", BAD_CAST "livejournal");
  }
  if (xmlSaveFormatFile(path, doc, TRUE) < 0) {
    g_set_error(err, 0, 0, "xmlSaveFormatFile error saving to %s.\n", path);
    goto out;
  }
  ret = TRUE;

out:
  if (path) g_free(path);
  if (doc) xmlFreeDoc(doc);

  return ret;
}
