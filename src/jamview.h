/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_JAM_VIEW_H__
#define __LOGJAM_JAM_VIEW_H__

#include "gtk-all.h"

#include "jamdoc.h"

typedef struct _JamView JamView;
typedef struct _JamViewClass JamViewClass;


/* these must match ACTION_VIEW_... in menu.c. */
/* these must match metas[] in jamview.c. */
typedef enum {
  JAM_VIEW_SUBJECT,
  JAM_VIEW_SECURITY,
  JAM_VIEW_MOOD,
  JAM_VIEW_PIC,
  JAM_VIEW_MUSIC,
  JAM_VIEW_LOCATION,
  JAM_VIEW_TAGS,
  JAM_VIEW_PREFORMATTED,
  JAM_VIEW_DATESEL,
  JAM_VIEW_COMMENTS,
  JAM_VIEW_SCREENING,
  JAM_VIEW_META_COUNT
} JamViewMeta;
#define JAM_VIEW_META_FIRST  JAM_VIEW_SECURITY
#define JAM_VIEW_META_LAST   JAM_VIEW_SCREENING

extern const char *jam_view_meta_to_name (JamViewMeta meta);
extern gboolean jam_view_meta_from_name (const char *name, JamViewMeta *meta);

#define JAM_TYPE_VIEW          (jam_view_get_type())
#define JAM_VIEW(object)       (G_TYPE_CHECK_INSTANCE_CAST((object), JAM_TYPE_VIEW, JamView))
#define JAM_VIEW_CLASS(klass)  (G_TYPE_CHECK_CLASS_CAST ((klass), JAM_TYPE_VIEW, JamViewClass))

extern GType jam_view_get_type (void);

extern GtkWidget *jam_view_new (JamDoc *doc);
extern GObject *jam_view_get_undomgr (JamView *view);
extern void jam_view_set_doc (JamView *view, JamDoc *doc);

extern void jam_view_settings_changed (JamView *view);

extern void jam_view_toggle_meta (JamView *view, JamViewMeta meta, gboolean show);
extern gboolean jam_view_get_meta_visible (JamView *view, JamViewMeta meta);

extern void jam_view_emit_conf (JamView *view);


#endif
