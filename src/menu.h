/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_MENU_H__
#define __LOGJAM_MENU_H__

#include "jam.h"


extern GtkWidget *menu_make_bar (JamWin *jw);

extern void menu_new_doc (JamWin *jw);
extern void menu_friends_manager (JamWin *jw);


#endif
