/* logjam - a GTK client for LiveJournal.
 * Copyright (C) 2000-2003 Evan Martin <evan@livejournal.com>
 */
#ifndef __LOGJAM_NETWORK_INTERNAL_H__
#define __LOGJAM_NETWORK_INTERNAL_H__

#include "network.h"


/* network-internal -- interface to http, used by network.
 * provides a blocking and nonblocking interface.
 * implemented by
 *   network-curl -- curl (unix, fork);
 *   network-win32 -- windows api (windows, threads).
 */

#define READ_BLOCK_SIZE  (2048)


typedef enum {
  NET_STATUS_NULL,
  NET_STATUS_BEGIN,
  NET_STATUS_SUCCESS,
  NET_STATUS_ERROR,
  NET_STATUS_PROGRESS,
  NET_STATUS_DONE
} NetStatusType;


typedef struct {
  guint32 current;
  guint32 total;
} NetStatusProgress;


typedef void (*NetStatusCallback) (NetStatusType status, gpointer statusdata, gpointer data);

extern GString *net_post_blocking (const char *url, GSList *headers, GString *post, NetStatusCallback cb, gpointer data, GError **err);

typedef void *NetMainloopHandle;

extern GString *net_post_mainloop (const char *url, GSList *headers, GString *post, NetStatusCallback cb, gpointer data, GError **err);
extern void net_mainloop_cancel (NetMainloopHandle handle);

extern gboolean net_verb_run_internal (LJVerb *verb, NetStatusCallback cb, gpointer data, GError **err);


#endif
